<?php include("../bd/conexion.php");?>

<?php include("../includes/header.php"); ?>
    <header class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../init.php">Home</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="indexAdmin.php">Administrador <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link btnVerCursos">Ver Cursos</a>
                    <a class="nav-item nav-link btnCrearCursos">Crear Cursos</a>
                    <a class="nav-item nav-link disabled">Editar</a>
                    <a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Desinscribir</a>
                </div>
            </div>
        </nav>
    </header><br>
    <div class="row">
        <div class="col-lg-12">
            <img src="../res/t3.jpg" height="310" width="1110">
        </div>
    </div><br>
    <div class="row">
        <div class="col-lg-12 mostrarCursos">
            <div class="card bg-dark text-white">
                <p class="h4">Cursos</p>
            </div><br>
            <table class="table table-bordered table_curso_alumno">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Curso</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Hrs Pedagógicas</th>
                        <th scope="col">Profesor</th>
                        <th scope="col">Cupos</th>
                        <th scope="col">Editar</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <?php
                $sql="SELECT curso.id, curso.nombre, curso.descripcion, curso.horas_pedagogicas, profesor.nombre, curso.capacidad FROM curso LEFT JOIN profesor ON curso.id=profesor.curso_id ORDER BY curso.nombre ASC";
                $result=mysqli_query($conexion, $sql);

                    while($mostrar=mysqli_fetch_array($result)){
                ?>
                <tbody>
                    <tr>
                        <td><?php echo $mostrar[1]?></td>
                        <td><?php echo $mostrar['descripcion']?></td>
                        <td><?php echo $mostrar['horas_pedagogicas']?></td>
                        <td><?php echo $mostrar[4]?></td>
                        <td><?php echo $mostrar['capacidad']?></td>
                        <td>
                            <a href="edit.php?id=<?php echo $mostrar['id']?>" class="btn btn-success">Editar
                            <i class="far fa-marker"></i>
                        </td>
                        <td>
                            <a href="delete.php?id=<?php echo $mostrar['id']?>" class="btn btn-danger">Eliminar
                            <i class="far fa-trash-alt"></i>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div><br><br>
    <div class="row agregarNuevoCurso">
        <div class="col-lg-12"><br>
            <div class="card bg-dark text-white">
                <br><p class="h5 title_CrearCurso">Crear Curso</p><br>
            </div>
            <form action="save.php" method="POST"><br>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" name="nombre" placeholder="Nombre Curso"> 
                    </div>
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" name="horas_pedagogicas" placeholder="Horas Pedagógicas" maxlength="3">
                    </div>
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" name="capacidad" placeholder="Capacidad" maxlength="3">
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="3" name="descripcion" placeholder="Descripción"></textarea>
                </div>
                <button class="btn btn-primary">Agregar</button><br>
            </form>
            <br>
        </div>
    </div>
</div>
<?php include("../includes/footer.php"); ?>