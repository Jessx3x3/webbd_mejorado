<?php

    include("../bd/conexion.php");

    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $query = "SELECT * FROM curso WHERE id = $id";
        $result = mysqli_query($conexion, $query);

        if(mysqli_num_rows($result) == 1){
            $row = mysqli_fetch_array($result);
            $nombre = $row['nombre'];
            $descripcion = $row['descripcion'];
            $horas_pedagogicas = $row['horas_pedagogicas'];
            $capacidad = $row['capacidad'];
        }
    }
    if(isset($_POST['update'])){
        $nombre = $_GET['nombre'];
        $descripcion = $_GET['descripcion'];
        $horas_pedagogicas = $_GET['horas_pedagogicas'];
        $capacidad = $_GET['capacidad'];

        $query = "UPDATE curso SET nombre = '$nombre', descripcion = '$descripcion', horas_pedagogicas = '$horas_pedagogicas', capacidad = '$capacidad' WHERE id = $id"; 
        mysqli_query($conexion, $query);

        $_SESSION['message'] = 'Alumno actualizado correctamente';
        $_SESSION['message_type'] = 'warning';

        header("Location: indexAdmin.php");
    }
?>

<?php include("../includes/header.php"); ?>
    <div class="container p-4">
        <div class="row">
            <div class="col-md-4 mx-auto">
                <div class="card card-body">
                    <form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST">
                        <div class="form-group">
                            <input type="text"  name="nombre" value="<?php echo $nombre;?>" class="form-control" placeholder="Actualiza Nombre">
                        </div>
                        <div class="form-group">
                            <input type="text" name="descripcion" value="<?php echo $descripcion;?>" class="form-control" placeholder="Actualiza Descripción">
                        </div>
                        <div class="form-group">
                            <input type="text" name="horas_pedagogicas" value="<?php echo $horas_pedagogicas;?>" class="form-control" placeholder="Actualiza Horas Pedagógicas">
                        </div>
                        <div class="form-group">
                            <input type="text" name="capacidad" value="<?php echo $capacidad;?>" class="form-control" placeholder="Actualiza Capacidad">
                        </div>
                        <button name="update" class="btn btn-success">Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include("../includes/footer.php"); ?>