<?php include("../bd/conexion.php");?>

<?php include("../includes/header.php"); ?>
    <header class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../init.php">Home</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="#">Estudiante <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="#">Ver Cursos</a>
                </div>
            </div>
        </nav>
    </header><br>
    <div class="row">
        <div class="col-lg-12">
            <img src="../res/s4.jpg" width="1110">
        </div>
    </div><br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <p class="display-4 title">Ver Cursos</p>
            </div><br>
            <table class="table table-bordered table_curso_alumno">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col" style="display:none;">Id</th>
                        <th scope="col">Curso</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Hrs Pedagógicas</th>
                        <th scope="col">Cupos</th>
                        <th scope="col">Profesor</th>
                        <th scope="col">Inscritos</th>
                    </tr>
                </thead>
                <?php
                    $sql="SELECT curso.id, curso.nombre, curso.descripcion, curso.capacidad, curso.horas_pedagogicas, profesor.nombre FROM curso LEFT JOIN profesor ON curso.id=profesor.curso_id ORDER BY curso.nombre ASC";
                    $result=mysqli_query($conexion, $sql);

                    while($mostrar=mysqli_fetch_array($result)){
                ?>
                <tbody>
                    <tr>
                        <td class="moduloId" style="display:none;"><?php echo $mostrar[0]?></td>
                        <td class="nameModule"><?php echo $mostrar[1]?></td>
                        <td class="descriptionModule"><?php echo $mostrar['descripcion']?></td>
                        <td class="hourModule"><?php echo $mostrar['horas_pedagogicas']?></td>
                        <td class="capacidadM"><?php echo $mostrar['capacidad']?></td>
                        <td><?php echo $mostrar[5]?></td>
                        <td>
                            <button type="button" class="btn btn-success btn_inscribir" data-target=".modal" data-toggle="modal">Inscribir</button>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inscripción de Ramo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>¿Estas segur@ que deseas cursar este ramo?</p>
                <p class="model_Id" name ="id" style="display:none;"></p>
                <p class="model_Nombre"></p>
                <p class="model_Horas"></p>
                <p class="model_Descripcion"></p>
                <p class="model_Capacidad" style="display:none"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>
                <button type="button" class="btn btn-primary confirmoStudent" id="send">Sí</button>
                <input type="text" placeholder="Ingrese Run" name="runStudentConfirmo" class="runStudentConfirmo">
            </div>
            </div>
        </div>
    </div>
<?php include("../includes/footer.php"); ?>