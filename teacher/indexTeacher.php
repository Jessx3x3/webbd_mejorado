    <?php 
    include("../bd/conexion.php");
    include("../includes/header.php");
    ?>
    <header class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../init.php">Home</a>
        </nav>
    </header>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <img src="../res/t1.jpg" width="1110">
        </div>
    </div><br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card bg-dark text-white">
                <p class="display-4 title"> Ver Cursos</p>
            </div><br>
            <table class="table table-bordered table_curso_alumno">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col" style="display:none;">Id</th>
                        <th scope="col">Curso</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Hrs Pedagógicas</th>
                        <th scope="col">Profesor</th>
                        <th scope="col">Alumnos Inscritos</th>
                        <th scope="col">Inscribirme</th>
                    </tr>
                </thead>
                <?php
                $sql="SELECT curso.id, curso.nombre, curso.descripcion, curso.horas_pedagogicas, profesor.nombre FROM curso LEFT JOIN profesor ON curso.id=profesor.curso_id ORDER BY curso.nombre ASC";

                $result=mysqli_query($conexion, $sql);

                while($mostrar=mysqli_fetch_array($result)){
                ?>
                <tbody>
                    <tr>
                        <td class="moduloId" style="display:none;"><?php echo $mostrar[0]?></td>
                        <td class="moduloT"><?php echo $mostrar[1]?></td>
                        <td class="descripT"><?php echo $mostrar['descripcion']?></td>
                        <td class="horasT"><?php echo $mostrar['horas_pedagogicas']?></td>
                        <td class="profeT"><?php echo $mostrar[4]?></td>
                        <td>
                            <button type="button" class="btn btn-success btn_info_inscritos">Alumnos</button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-info btn_info_teacher" data-target=".modal" data-toggle="modal">Inscribirme</button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
            <div class="col-lg-12 tabla_alumnosMatriculados">
                    <div class="card bg-dark text-white">
                        <p class="display-4 title title_alumnosCursos"></p>
                    </div><br>
                    <table class="table table-bordered table_curso_alumno">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Apellido</th>
                                <th scope="col">Run</th>
                            </tr>
                        </thead>

                        <?php
                        $sql="SELECT curso.id, alumno.nombre, alumno.apellido, alumno.run FROM curso LEFT JOIN alumno ON curso.id=alumno.curso_id";

                        $result=mysqli_query($conexion, $sql);

                        while($mostrar=mysqli_fetch_array($result)){
                        ?>

                        <tbody>
                            <tr>
                                <td><?php echo $mostrar[1]?></td>
                                <td><?php echo $mostrar[2]?></td>
                                <td><?php echo $mostrar[3]?></td>
                            </tr>

                        <?php } ?>   

                        </tbody>
                    </table>
            </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inscripción de Ramo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>¿Estas segur@ que deseas impartir este ramo?</p>
                <p class="modelId" name ="id" style="display:none;"></p>
                <p class="modelNombre"></p>
                <p class="modelHoras"></p>
                <p class="modelDescripcion"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>
                <button type="button" class="btn btn-primary confirmo" id="send">Sí</button>
                <input type="text" placeholder="Ingrese Run" name="runConfirmo" class="runConfirmo">
            </div>
            </div>
        </div>
    </div>


<?php include("../includes/footer.php"); ?>