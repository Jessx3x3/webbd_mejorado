$(document).ready(function(){
    $(".col_tabla_alumno").hide();
    $(".cardInscripcion, .mostrarCursos, .agregarNuevoCurso").hide();
    $(".cardStudentIn, .cardTeacherIn").hide();
    $(".tabla_alumnosMatriculados").hide();

    $(".btn_info").click(function(){
        $('.col_tabla_alumno').toggle();
    });

    $(".btn_info_inscritos").click(function(){
        $(".title_alumnosCursos").text($(this).parent().siblings(".moduloT").text());
        $(".tabla_alumnosMatriculados").show();
    });

    $(".btnStudentLog").click(function(){
        $(".cardStudentIn").toggle();
    });

    $(".btnTeacherLog").click(function(){
        $(".cardTeacherIn").toggle();
    });

    $(".btnVerCursos, .btnEditarCursos").click(function(){
        $(".mostrarCursos").toggle();
    });

    $(".btnCrearCursos").click(function(){
        $(".agregarNuevoCurso").toggle();
    });

    $(".btn_inscribir").click(function(){
       $(".model_Id").text($(this).parent().siblings(".moduloId").text());
       $(".model_Nombre").text($(this).parent().siblings(".nameModule").text());
       $(".model_Horas").text($(this).parent().siblings(".hourModule").text());
       $(".model_Descripcion").text($(this).parent().siblings(".descriptionModule").text());
       $(".model_Capacidad").text($(this).parent().siblings(".capacidadM").text());
       $(".runStudentConfirmo").hide();
    });
    $(".btn_info_teacher").click(function(){
       $(".modelId").text($(this).parent().siblings(".moduloId").text());
       $(".modelNombre").text($(this).parent().siblings(".moduloT").text());
       $(".modelHoras").text($(this).parent().siblings(".horasT").text());
       $(".modelDescripcion").text($(this).parent().siblings(".descripT").text());
       $(".runConfirmo").hide();
    });
    $("select.cursoSelect").change(function(){

        var moduleName = $(this).children("option:selected").text();
        
        $(".cardInscripcion").show();
        $(".moduloInscripcion").text(moduleName);
    });
    
    $(".confirmo").click(function(){

        $(".runConfirmo").show();
        var id = $(".modelId").text();
        var runProfesor = $(".runConfirmo").val();
        
        $.ajax({
            url: 'in.php',
            type: 'post',
            data: {idModulo: id, runProfesor: runProfesor},
        });

        if(id != "" && runProfesor != ""){
            $('.modal').modal('hide');
        }

    });
    $(".confirmoStudent").click(function(){

        $(".runStudentConfirmo").show();
        var id = $(".model_Id").text();
        var runStudent = $(".runStudentConfirmo").val();
        var capacidadStudent = $("model_Capacidad").text();

        $.ajax({
            url: 'inStudent.php',
            type: 'post',
            data: {idModulo: id, run: runStudent, capacidad: capacidadStudent},
        });
        
        if(id != "" && runStudent != ""){
            $('.modal').modal('hide');
        }
        
        
    });

});
