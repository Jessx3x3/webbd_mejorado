<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="JS/script.js"></script>
    <title>Mantenimiento y Evaluación</title>
</head>
<body>
    <div id="content" role="main" class="container-md">
        <header class="header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
                <a class="navbar-brand" href="init.php">Ingresa</a>
                <div class="collapse navbar-collapse" id="navbarsExample05">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="init.php">Inicial<span class="sr-only">(current)</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header><br>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="res/s2.jpg" height="350" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="res/t1.jpg" height="350" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="res/s4.jpg" height="350" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="res/t3.jpg" height="350" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div><br>
        <div class="row">
            <div class="col-lg-4">
                <div class="card cardStudent"><br>
                    <h5 class="card-title">Estudiante</h5>
                    <p class="card-text">Ingresa para ver y tomar módulo</p><br>
                    <a href="#" class="btn btn-primary btnStudentLog">Ingresa</a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card cardTeacher"><br>
                    <h5 class="card-title">Profesor</h5>
                    <p class="card-text">Ingresa para ver en qué modulo puedes realizar clases</p>
                    <a href="#" class="btn btn-primary btnTeacherLog">Ingresa</a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card cardAdmin"><br>
                    <h5 class="card-title">Administrador</h5>
                    <p class="card-text">CRUD</p><br>
                    <a href="admin/indexAdmin.php" class="btn btn-primary btnAdminLog">Ingresa</a>
                </div>
            </div>
        </div><br>
        <div class="row"><br>
            <div class="col-lg-4">
                <div class="card cardStudentIn">
                    <h5 class="card-title">Ingresa</h5>
                    <form method="post" action="student/login.php">
                        <label for="studentNameLog">Primer Nombre</label>
                        <input type="text" class="form-control" id="studentNameLog" name="studentNameLog"><br>

                        <label for="studentRunLog">Run</label>
                        <input type="text" class="form-control" id="studentRunLog" name="studentRunLog"><br>
        
                        <button class="btn btn-primary">Iniciar</button><br>
                    </form>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card cardTeacherIn">
                    <h5 class="card-title">Ingresa</h5>
                    <form method="post" action="teacher/login.php">
                        <label for="teacherNameLog">Primer Nombre</label>
                        <input type="text" class="form-control" id="teacherNameLog" name="teacherNameLog"><br>

                        <label for="teacherRunLog">Run</label>
                        <input type="text" class="form-control" id="teacherRunLog" name="teacherRunLog"><br>
        
                        <button class="btn btn-primary">Iniciar</button><br>
                    </form>
                </div>
            </div>
            <div class="col-lg-4">
                
            </div>
        </div>

        <style type="text/css">
            body { background-image: url("res/b2.jpg"); }
            .header .navbar { background-color: transparent !important;}
            .cardStudent, .cardTeacher, .cardAdmin, .cardStudentIn, .cardTeacherIn, .cardAdminIn{
                padding: 5%;
                background-color: teal;
                color: white;
            } 
        </style>

<?php include("includes/footer.php"); ?>

